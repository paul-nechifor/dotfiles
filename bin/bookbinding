#!/bin/bash

set -e

main() {
    local input="$1"
    local random_prefix
    random_prefix="bookbinding-$(random_name)"

    pdfjam --outfile "$random_prefix-prepended.pdf" "$input" "{},{},1-"
    input="$random_prefix-prepended.pdf"

    local n_pages
    n_pages="$(pdfinfo "$input" | awk '/^Pages:/ {print $2}')"
    local signature_size=4
    local pages_in_signature=$((4 * signature_size))
    local n_signatures="$(( n_pages / pages_in_signature ))"
    local sig_pages_odd
    sig_pages_odd="$(seq 1 2 $((pages_in_signature / 2)) | comma_join)"
    local sig_pages_even
    sig_pages_even="$(seq 2 2 $((pages_in_signature / 2)) | comma_join)"

    if [[ "$(( n_pages % pages_in_signature ))" -gt 0 ]]; then
        n_signatures=$((n_signatures+1))
    fi
    local n_final_sheets=$(( n_signatures * signature_size ))

    say "Pages: $n_pages"
    say "Signature size: $signature_size"
    say "Number of signatures: $n_signatures"
    say "Prefix used: $random_prefix"
    say "Number of final sheets: $n_final_sheets"

    local i=1
    local s=0
    local face_odd=()
    local face_even=()

    while [[ $i -lt $n_pages ]]; do
        local end=$(( i + pages_in_signature - 1 ))
        if [[ $end -gt n_pages ]]; then
        end="$n_pages,$( seq $(( pages_in_signature - ( n_pages % pages_in_signature ) )) | sed 's/.*/{}/' | comma_join )"
        fi
        pdfjam --outfile "$random_prefix-signature-$s.pdf" "$input" "$i-$end"
        pdfbook --outfile "$random_prefix-book-$s.pdf" --signature $signature_size "$random_prefix-signature-$s.pdf"
        face_odd+=("$random_prefix-book-$s.pdf" "$sig_pages_odd")
        face_even+=("$random_prefix-book-$s.pdf" "$sig_pages_even")
        i=$(( i + pages_in_signature ))
        s=$(( s + 1 ))
    done

    pdfjam --landscape --outfile out-odd.pdf "${face_odd[@]}"
    pdfjam --landscape --outfile out-even-prereversed.pdf "${face_even[@]}"
    pdfjam --landscape --outfile out-even.pdf out-even-prereversed.pdf "$(seq "$n_final_sheets" -1 1 | comma_join)"

    for f in even odd; do
      local m=72 # 72 is 1 american inch
      pdfjam --fitpaper true --trim "-4cm -4cm -4cm -4cm" out-"$f".pdf -o out-"$f"-margin.pdf
      mv out-"$f"-margin.pdf out-"$f".pdf
    done

    rm -f ./"$random_prefix"-* out-even-prereversed.pdf
}

say() {
    tput setaf 4; echo "$@"; tput sgr0
}

comma_join() {
    tr '\n' , | sed s/.$//
}

random_name() {
    tr -cd 'a-zA-Z0-9' < /dev/urandom | head -c 10
}

main "$@"
