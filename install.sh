#!/usr/bin/env bash

set -ex

install_source="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
install_script="$install_source/$(basename "${BASH_SOURCE[0]}")"

main() {
  determine_environment

  if [[ $1 ]]; then
    "$@"
    return
  fi
  if [[ $own_computer ]]; then
    root_start
  else
    user_start
  fi
}

infect() {
  check_for_requirements
  if [[ ! -e "$install_dir" ]]; then
    echo 'Cloning dotfiles repo...'
    git clone https://gitlab.com/paul-nechifor/dotfiles "$install_dir"
  else
    echo 'Repo exists. Trying to fetch...'
    cd "$install_dir"
    if git diff-index --quiet HEAD; then
      git checkout master
      git fetch --all
      if ! git merge --ff-only; then
        echo 'Failed to ff-only merge. Resolve it manually.'
        exit 1
      fi
    echo
      echo 'Cannot update because of local changes. ' \
        'Continuing with local changes...'
    fi
  fi

  echo 'Starting installation...'
  bash "$install_dir"/install.sh

  echo -e "\033[33m☢ \033[0m Infection complete. \033[33m☢ \033[0m"
}

determine_environment() {
  if [[ ! $ran_before ]]; then
    echo 'Determining environment...'
    export ran_before=true
  fi

  set +e

  is_vagrant="$(id -u vagrant 2>/dev/null && echo 1)"
  if [[ ! $username ]]; then
    username=$(whoami)
  fi

  if [[ $(id -u) -eq 0 ]]; then
    install_dir="$(su "$username" -c "echo \$HOME")/.dotfiles"
  else
    install_dir="$HOME/.dotfiles"
  fi
  export config_dir="$install_dir/config"

  is_ubuntu=$(grep Ubuntu /etc/issue && echo 1)
  is_mint=$(grep Mint /etc/issue && echo 1)
  is_raspbian=$(grep Raspbian /etc/issue && echo 1)
  is_centos=$(grep CentOS /etc/issue && echo 1)
  own_computer=$([[ ! $is_centos ]] && echo 1)

  export is_vagrant install_dir config_dir is_ubuntu is_raspbian is_mint
  export is_centos own_computer

  set -e
}

check_for_requirements() {
  local to_install=""

  if [[ ! $(unzip -v 2>/dev/null) ]]; then
    to_install+=" unzip"
  fi

  if ! which git &>/dev/null; then
    to_install+=" git"
  fi

  if [[ ! "$to_install" ]]; then
    return
  fi

  echo 'Trying to install requirements...'

  if [[ $is_ubuntu || $is_raspbian || $is_mint ]]; then
    sudo -SE apt-get -y install -qq $to_install
  elif [[ $is_centos ]]; then
    sudo -SE yum -y install $to_install
  else
    echo "Could not find or install requirements: $to_install"
    exit 1
  fi
}

root_start() {
  local exports=(
    "ran_before=$ran_before"
    "http_proxy=$http_proxy"
    "https_proxy=$https_proxy"
    "username=$username"
  )
  if [[ $(id -u) -ne 0 ]]; then
    echo 'Switching to root...'
    sudo -SE su -c "${exports[*]} bash '$install_script' root_start"
    return
  fi

  check_for_requirements

  create_user
  link_root_files
  su "$username" -c "${exports[*]}; bash '$install_script' link_user_files"

  if [[ $is_ubuntu ]]; then
    if [[ $(grep -o '[0-9]*' /etc/issue | head -n1) -ge 20 ]]; then
      bash "$install_source"/provision/ubuntu20.sh
    else
      bash "$install_source"/provision/ubuntu18.sh
    fi
  elif [[ $is_raspbian ]]; then
    bash "$install_source"/provision/raspbian.sh
  elif [[ $is_mint ]]; then
    bash "$install_source"/provision/mint.sh
  fi
}

user_start() {
  check_for_requirements
  link_user_files
}

create_user() {
  if id -u "$username" >/dev/null; then
    return
  fi
  echo "Creating user ${username}..."
  adduser "$username" --home "/home/$username"
}

link_root_files() {
  echo 'Linking root files...'
  link_common_files

  rm -f /usr/share/X11/xkb/symbols/ro
  ln -s "$config_dir/xkb/layout" /usr/share/X11/xkb/symbols/ro
  dpkg-reconfigure xkb-data
}

# Remove a directory (relative to home) and recreate it.
wipeout() {
  rm -fr "${HOME:?}/$1"
  mkdir -p "$HOME/$1" 2>/dev/null || true
}

link_file() {
  rm -f "$HOME/$2"
  ln -s "$config_dir/$1" "$HOME/$2"
}

link_user_files() {
  echo 'Linking user files...'
  link_common_files

  link_file ack/rc .ackrc
  link_file git/config .gitconfig
  link_file git/ignore .gitignore
  link_file input/inputrc .inputrc
  link_file tmux/tmux.conf .tmux.conf
  link_file x/modmap .Xmodmap
  link_file x/resources .Xresources
  link_file gdb/gdbinit .gdbinit
  link_file lvim/config.lua .config/lvim/config.lua

  wipeout .i3
  wipeout .config/i3
  link_file i3/config .i3/config
  link_file i3/config .config/i3/config

  wipeout .config/i3status
  link_file i3/status .config/i3status/config

  mkdir -p "$HOME/.cmus"
  link_file cmus/autosave .cmus/autosave

  wipeout .config/dunst
  link_file dunst/rc .config/dunst/dunstrc

  link_file python/pythonrc.py .pythonrc

  mkdir -p ~/.subversion >/dev/null 2>&1
  link_file svn/config .subversion/config

  provision_vim

  if [[ $DISPLAY ]]; then
    setxkbmap ro || true
  fi

  if [[ $is_centos ]]; then
    bash "$install_dir/provision/centos6.sh"
  fi

  touch ~/.hushlogin

  install_fonts
}

link_common_files() {
  rm -f ~/.bashrc
  ln -s "$config_dir/bash/bashrc.sh" ~/.bashrc

  rm -f ~/.vimrc
  ln -s "$config_dir/vim/vimrc.vim" ~/.vimrc

  rm -f ~/.vim-spellfile.utf8.add
  ln -s "$config_dir/vim/spellfile" ~/.vim-spellfile.utf8.add

  rm -f ~/.dircolors
  ln -s "$config_dir/dircolors/dircolors" ~/.dircolors
}

install_fonts() {
  rm -fr ~/tmp/fonts-install
  mkdir -p ~/tmp/fonts-install
  (
    cd ~/tmp/fonts-install
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/SourceCodePro.tar.xz
    tar -xf SourceCodePro.tar.xz
    mkdir -p ~/.fonts
    mv *.ttf ~/.fonts
    fc-cache -fv
  )
  rm -fr ~/tmp/fonts-install
}

create_vim_structure() {
  echo '  Recreating Vim dir structure...'
  mkdir ~/.vimswap ~/.vimundo 2>/dev/null || true
  rm -fr ~/.vim
  mkdir ~/.vim
  cd ~/.vim
  mkdir autoload bundle doc plugin syntax
}

wgetq() {
  if [[ $2 ]]; then
    wget -q "$1" -O- > "$2"
  else
    wget -q "$1"
  fi
}

get_pathogen() {
  cd ~/.vim/autoload
  echo '  Downloading pathogen...'
  wgetq https://raw.github.com/tpope/vim-pathogen/master/autoload/pathogen.vim
}

wget_master() {
  wgetq "https://github.com/$1/archive/master.zip" master.zip
  unzip -q master.zip
  rm master.zip
}

install_from_github() {
  cd ~/.vim/bundle
  echo "  Downloading module $1..."
  wget_master "$2"
  mv ./*-master "$1"
}

install_vim_modules() {
  install_from_github ack mileszs/ack.vim
  install_from_github coffee-script kchmck/vim-coffee-script
  install_from_github commentary tpope/vim-commentary
  install_from_github ctrlp ctrlpvim/ctrlp.vim
  install_from_github easymotion Lokaltog/vim-easymotion
  install_from_github fugitive tpope/vim-fugitive
  # install_from_github gitgutter airblade/vim-gitgutter
  install_from_github jade digitaltoad/vim-jade
  install_from_github javascript pangloss/vim-javascript
  install_from_github markdown tpope/vim-markdown
  install_from_github matchem ervandew/matchem
  install_from_github nerdtree scrooloose/nerdtree
  install_from_github perl vim-perl/vim-perl
  install_from_github rename danro/rename.vim
  install_from_github scss cakebaker/scss-syntax.vim
  install_from_github stylus wavded/vim-stylus
  install_from_github supertab ervandew/supertab
  install_from_github svn-blame paul-nechifor/vim-svn-blame
  install_from_github syntastic scrooloose/syntastic
  install_from_github textmanip t9md/vim-textmanip
  install_from_github tmux-navigator christoomey/vim-tmux-navigator
  install_from_github typescript-vim leafgarland/typescript-vim
}

provision_vim() {
  echo 'Provisioning Vim...'
  create_vim_structure
  get_pathogen
  install_vim_modules
  echo 'Provisioning Vim complete.'
}

main "$@"
