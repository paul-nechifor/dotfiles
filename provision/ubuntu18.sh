#!/bin/bash

set -e
set -x

root=$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)

lang_pack="ro"
username="$(getent passwd 1000 | cut -d: -f1)"
export DEBIAN_FRONTEND=noninteractive

remove_list=(
    unity-lens-shopping
    unity-scope-musicstores
    unity-scope-video-remote
    cmdtest
)

install_list=(
    # openjdk-8-jdk
    # ttf-'*'
    apt-file
    arandr
    audacity # Audio editor.
    atop # Performance monitor.
    bridge-utils # For networking.
    build-essential # Important for compiling packages.
    calibre # Ebook program.
    cheese # Webcam program.
    cifs-utils # Needed to mount Samba file systems.
    cloc # Counts lines of code.
    cmus
    cryptsetup
    curl
    darktable
    dconf-editor
    dkms
    dos2unix
    dosbox
    easytag # MP3 tag editor.
    exfat-fuse # To read SD card filesystems.
    exfat-utils # To read SD card filesystems.
    expect # Talk to interactive scripts
    festival
    flameshot # For taking screenshots.
    flatpak # To install applications in isolation.
    fonts-firacode
    fonts-noto
    gimp
    git
    git-cola
    git-extras
    git-svn
    gitg
    gnome-shell
    gnome-tweak-tool
    golang
    gource
    gparted # Partition editor.
    grc
    htop # Better than top.
    i3
    i3lock
    icedax
    id3tool
    imagemagick
    inkscape
    inotify-tools
    ipython
    kdenlive
    keychain
    kompare
    lame
    language-pack-$lang_pack
    language-pack-$lang_pack-base
    language-pack-gnome-$lang_pack
    language-pack-gnome-$lang_pack-base
    libblockdev-crypto2 # Because of a LUKS error.
    libmad0
    libmtp-common
    libmtp-dev
    libmtp-runtime
    libmtp9
    linux-headers-$(uname -r)
    linux-headers-generic
    lm-sensors
    maven
    mencoder
    mpg321
    mtp-tools
    nasm
    nautilus-script-audio-convert
    netbeans
    network-manager-openvpn
    network-manager-openvpn-gnome
    nfs-common
    nodejs
    openvpn
    p7zip
    p7zip-full
    p7zip-rar
    pidgin
    python-dev # Needed to build some Python packages.
    python-gi-cairo # Needed by space-hoarder
    python-jedi
    python-pip
    python3-dev # Needed to build some Python packages.
    qutebrowser
    rename
    retext
    rofi # To run commands.
    rxvt-unicode-256color
    s3cmd
    scrot
    shellcheck
    silversearcher-ag
    smbclient
    subversion
    thunar
    thunar-archive-plugin
    thunar-media-tags-plugin
    tidy
    tig
    tmux
    tree
    ttf-mscorefonts-installer
    tumbler-plugins-extra
    ubuntu-restricted-extras
    unrar
    vim
    vim-gtk
    virtualbox-guest-additions-iso
    virtualbox-qt
    virtualenv
    vlc # Video player.
    wicd-gtk
    xbacklight
    xchm
    xdotool
    xsel
)

npm_packages=(
    coffee-script
    eslint
    gulp
    yarn
)

pip_packages=(
    flake8
)

unnecessary_files=(
    Desktop
    Documents Documente
    Downloads Descărcări
    Music Muzică
    Pictures Poze
    Public
    Templates Șabloane
    Videos Video
    examples.desktop
)

gsettings_values=(
    org.gnome.desktop.background picture-options 'none'
    org.gnome.desktop.background picture-uri ''
    org.gnome.desktop.background primary-color '#45a2570a4b04'
    org.gnome.desktop.background show-desktop-icons false
    org.gnome.desktop.interface cursor-theme 'DMZ-Black'
    org.gnome.desktop.interface document-font-name 'Sans 9'
    org.gnome.desktop.interface font-name 'Ubuntu 9'
    org.gnome.desktop.interface gtk-theme 'Radiance'
    org.gnome.desktop.interface monospace-font-name 'Ubuntu Mono 11'
    org.gnome.desktop.wm.preferences theme 'Adwaita'
    org.gnome.desktop.wm.preferences titlebar-font 'Ubuntu Bold 9'
    org.gnome.gedit.preferences.editor scheme 'oblivion'
    org.gnome.gedit.preferences.editor use-default-font false
)

main() {
    if [[ $1 ]]; then
        subcommand_"$1"
    else
        subcommand_desktop_root
    fi
}

subcommand_desktop_root() {
    add_ppas_and_update
    remove_packages
    preconfigure_packages
    install_packages
    install_non_system_packages
    install_i3_gnome
    switch_to_user
}

add_ppas_and_update() {
    apt-get update
}

remove_packages() {
    apt-get remove -y "${remove_list[@]}"
}

preconfigure_packages() {
    echo '
gdm3	gdm3/daemon_name	string	/usr/sbin/gdm3
gdm3	shared/default-x-display-manager	select	lightdm
libssl1.0.0	libssl1.0.0/restart-failed	error	
libssl1.0.0	libssl1.0.0/restart-services	string	
mathematica-fonts	mathematica-fonts/accept_license	boolean	true
mathematica-fonts	mathematica-fonts/http_proxy	string	
mathematica-fonts	mathematica-fonts/license	note	
openvpn	openvpn/create_tun	boolean	false
texlive-base	texlive-base/binary_chooser	multiselect	pdftex, dvips, dvipdfmx, xdvi
texlive-base	texlive-base/texconfig_ignorant	error	
ttf-mscorefonts-installer	msttcorefonts/accepted-mscorefonts-eula	boolean	false
ttf-mscorefonts-installer	msttcorefonts/baddldir	error	
ttf-mscorefonts-installer	msttcorefonts/dldir	string	
ttf-mscorefonts-installer	msttcorefonts/dlurl	string	
ttf-mscorefonts-installer	msttcorefonts/error-mscorefonts-eula	error	
ttf-mscorefonts-installer	msttcorefonts/present-mscorefonts-eula	note	
ttf-root-installer	ttf-root-installer/baddldir	error	
ttf-root-installer	ttf-root-installer/blurb	note	
ttf-root-installer	ttf-root-installer/dldir	string	
ttf-root-installer	ttf-root-installer/savedir	string	
wicd-daemon	wicd/users	multiselect
' | debconf-set-selections
}

install_packages() {
    wget -qO- https://deb.nodesource.com/setup_12.x | sudo -E bash -

    apt-get install -y debconf-utils
    apt-get upgrade -y
    apt-get install "${install_list[@]}" -y
    apt-get autoremove

    wget https://github.com/wagoodman/dive/releases/download/v0.9.1/dive_0.9.1_linux_amd64.deb
	sudo apt install dive_0.9.1_linux_amd64.deb
    rm dive_0.9.1_linux_amd64.deb
}

install_non_system_packages() {
    npm install -g "${npm_packages[@]}"
}

install_i3_gnome() {
    (
        cd "$root/vendor/i3-gnome";
        make install
    )
}

switch_to_user() {
    su "$username" -c "bash $BASH_SOURCE desktop_user"
}

subcommand_desktop_user() {
    set_gsettings
    set_dconf
    configure_dirs
    install_user_things
}

set_gsettings() {
    local i

    local a=("${gsettings_values[@]}")
    for (( i=0; i<"${#a[@]}"; i+=3)); do
        gsettings set "${a[i]}" "${a[i+1]}" "${a[i+2]}"
    done

    export PATH="$HOME/.dotfiles/bin:$PATH"
    tm 11
    tm gruv
}

set_dconf() {
    local profile=':b1dcc9dd-5262-4d8d-a863-c897e6d979b9'
    dconf write /org/gnome/terminal/legacy/profiles:/"$profile"/scrollbar-policy '"never"'
    dconf write /org/gnome/terminal/legacy/default-show-menubar false
}

configure_dirs() {
    cd

    # Delete annyoing home dir structure.
    rm -fr "${unnecessary_files[@]}"
    cd -

    # Create main dirs.
    mkdir -p data eth pro

    # Create backups dir.
    mkdir -p data/backup
}

install_user_things() {
    pip install -U --user "${pip_packages[@]}"
}

main "$@"
