#!/usr/bin/env bash

set -ex

install_path="$HOME/.ownbin"
tmpdir=$(mktemp -dt provisionXXXXXXXX)

main() {
  # You have to `export root_req=1` before running the script in order to
  # install the root packages required if they are not present.
  [[ $root_req ]] && install_root_requirements

  which gcc &>/dev/null || die 'No GCC found.'

  [[ -d "$install_path" ]] || mkdir -p "$install_path"
  install_python || soft_fail 'Failed to install Python.'
  install_vim || soft_fail 'Failed to install Vim'
  install_tmux || soft_fail 'Failed to install Tmux'
  install_git || soft_fail 'Failed to install Git'
  rm -fr "$tmpdir"
}

install_root_requirements() {
  local packages=(
    ack
    asciidoc
    bash-completion
    bzip2-devel
    compat-readline5
    curl-devel
    expat-devel
    gettext-devel
    glib2-devel
    glibc-static
    gnutls-devel
    htop
    ipython
    libuv
    libuv-devel
    libxslt
    libxslt-devel
    lsof
    mysql
    mysql-devel
    mysql-server
    ncurses
    ncurses-devel
    nodejs
    npm
    openldap-clients
    openldap-devel
    openssl-devel
    perl-devel
    readline-devel
    realpath
    redis
    sqlite-devel
    the_silver_searcher
    tmux
    tree
    vim-common
    vim-enhanced
    xmlsec1
    xmlsec1-devel
    xmlto
    xz-devel
    zlib-devel
  )

  cd /tmp
  local file="epel-release-6-8.noarch.rpm"
  wget -q "http://dl.fedoraproject.org/pub/epel/6/x86_64/$file"
  rpm -Uvh "$file" >/dev/null 2>&1 || true
  rm "$file"

  sudo yum clean all
  sudo yum -y shell <<<"
    update
    groupinstall 'Development tools'
    install ${packages[@]}
    run
  "
}

install_python() {
  local version=2.7.9

  [[ -e "$install_path/bin/python2.7" ]] && return

  cd "$tmpdir"
  wgetf https://www.python.org/ftp/python/$version/Python-${version}.tgz Python.tgz
  tar -xzf Python.tgz
  cd Python-$version
  ./configure --prefix="$install_path"
  make && make altinstall

  wgetf https://bootstrap.pypa.io/ez_setup.py ez_setup.py
  "$install_path/bin/python2.7" ez_setup.py
  "$install_path/bin/easy_install-2.7" pip
  "$install_path/bin/pip2.7" install virtualenv jedi flake8
  ln -s "$install_path/bin/python2.7" "$install_path/bin/python"
  ln -s "$install_path/bin/python2.7" "$install_path/bin/python2"
}

install_vim() {
  [[ -e "$install_path/bin/vim" ]] && return

  cd "$tmpdir"
  local vim_source="https://github.com/vim/vim/archive/v8.0.1444.zip"
  wgetf "$vim_source" vim
  unzip -q vim
  cd vim-8*
  local vim_options=(
    --disable-gui
    --disable-netbeans
    --enable-cscope
    --enable-largefile
    --enable-multibyte
    --enable-pythoninterp
    --with-features=huge
    --with-python-config-dir="$install_path/lib/python2.7/config"
    --without-x
  )
  ./configure "${vim_options[@]}" --prefix="$install_path"
  make install
  cd ..
  rm -fr vim-8*
}

install_tmux() {
  [[ -e "$install_path/bin/tmux" ]] && return

  cd "$tmpdir"
  local libevent="libevent-2.0.21-stable"
  wgetf "https://github.com/downloads/libevent/libevent/${libevent}.tar.gz" libevent.tar.gz
  tar -xzf libevent.tar.gz
  cd "$libevent"
  ./configure --prefix="$install_path"
  make && make install

  cd "$tmpdir"
  wgetf https://github.com/tmux/tmux/releases/download/2.6/tmux-2.6.tar.gz tmux.tar.gz
  tar -xzf tmux.tar.gz
  cd tmux-2.6
  ./configure CFLAGS="-I$install_path/include" LDFLAGS="-L$install_path/lib" --prefix="$install_path"

  make -j"$(nproc)" && make install
}

install_git() {
    [[ -e "$install_path/bin/git" ]] && return

    cd "$tmpdir"
	wgetf https://github.com/git/git/archive/v2.20.0.tar.gz git.tar.gz
    tar -xzf git.tar.gz
    cd git-*
    make prefix="$install_path" all
    make prefix="$install_path" install
}

install_ag() {
    wget http://ftp.cs.stanford.edu/mirrors/exim/pcre/pcre-8.38.tar.bz2
    tar xvf pcre-8.38.tar.bz2
    rm -fr pcre-8.38.tar.bz2
    (
        cd pcre-8.38/
        ./configure --prefix="$HOME/pcre" --enable-jit --enable-unicode-properties
        make
        make install
    )
    rm -fr pcre-8.38

    git clone https://github.com/ggreer/the_silver_searcher.git
    (
        cd the_silver_searcher/
        aclocal && autoconf && autoheader && automake --add-missing
        ./configure PCRE_CFLAGS="-I $HOME/pcre/include" PCRE_LIBS="-L $HOME/pcre/lib -Wl,-Bstatic -lpcre -Wl,-Bdynamic"
        make
    )
}

wgetf() {
  wget -q --no-check-certificate "$1" -O- > "$2"
}

soft_fail() {
  echo "$(tput setaf 1)$1$(tput sgr0)"
}

die() {
    soft_fail "$@"
    exit 1
}

main
