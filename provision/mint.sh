#!/bin/bash

set -e
set -x

root=$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)

username="$(getent passwd 1000 | cut -d: -f1)"
export DEBIAN_FRONTEND=noninteractive

install_list=(
    apt-file
    arandr
    audacity # Audio editor.
    atop # Performance monitor.
    bridge-utils # For networking.
    build-essential # Important for compiling packages.
    calibre # Ebook program.
    cheese # Webcam program.
    cifs-utils # Needed to mount Samba file systems.
    cloc # Counts lines of code.
    cmus
    cryptsetup
    curl
    darktable
    dconf-editor
    dkms
    dos2unix
    dosbox
    easytag # MP3 tag editor.
    exfat-fuse # To read SD card filesystems.
    #exfat-utils # To read SD card filesystems.
    expect # Talk to interactive scripts
    festival
    flameshot # For taking screenshots.
    flatpak # To install applications in isolation.
    fonts-firacode
    fonts-noto
    gimp
    git
    git-cola
    git-extras
    git-svn
    gitg
    gnome-shell
    # gnome-tweak-tool
    golang
    gource
    gparted # Partition editor.
    grc
    htop # Better than top.
    i3
    i3lock
    icedax
    id3tool
    imagemagick
    inkscape
    inotify-tools
    kdenlive
    keychain
    kompare
    lame
    libblockdev-crypto2 # Because of a LUKS error.
    libmad0
    libmtp-common
    libmtp-dev
    libmtp-runtime
    libmtp9
    linux-headers-$(uname -r)
    linux-headers-generic
    lm-sensors
    maven
    mencoder
    mpg321
    mtp-tools
    nasm
    nautilus-script-audio-convert
    network-manager-openvpn
    network-manager-openvpn-gnome
    nfs-common
    openvpn
    p7zip
    p7zip-full
    p7zip-rar
    pidgin
    rename
    retext
    rofi # To run commands.
    #rxvt-unicode-256color
    s3cmd
    scrot
    shellcheck
    silversearcher-ag
    smbclient
    subversion
    thunar
    thunar-archive-plugin
    thunar-media-tags-plugin
    tidy
    tig
    tmux
    tree
    ttf-mscorefonts-installer
    tumbler-plugins-extra
    ubuntu-restricted-extras
    unrar
    vim
    vim-gtk
    virtualbox-guest-additions-iso
    virtualbox-qt
    vlc # Video player.
    xbacklight
    xchm
    xdotool
    xsel
    xfce4-appfinder # App finder.

    # Needed by asdf ----------------------------------------------
    # For nodejs
    dirmngr gpg curl gawk
    # For python
    make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev
    libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev
    libxmlsec1-dev libffi-dev liblzma-dev

    # For Docker.
    docker-ce docker-ce-cli containerd.io
)

unnecessary_files=(
    Desktop
    Documents Documente
    Downloads Descărcări
    Music Muzică
    Pictures Poze
    Public
    Templates Șabloane
    Videos Video
    examples.desktop
)

gsettings_values=(
    org.gnome.desktop.background picture-options 'none'
    org.gnome.desktop.background picture-uri ''
    org.gnome.desktop.background primary-color '#45a2570a4b04'
    org.gnome.desktop.background show-desktop-icons false
    org.gnome.desktop.interface cursor-theme 'DMZ-Black'
    org.gnome.desktop.interface document-font-name 'Sans 9'
    org.gnome.desktop.interface font-name 'Ubuntu 9'
    org.gnome.desktop.interface gtk-theme 'Radiance'
    org.gnome.desktop.interface monospace-font-name 'Ubuntu Mono 11'
    org.gnome.desktop.wm.preferences theme 'Adwaita'
    org.gnome.desktop.wm.preferences titlebar-font 'Ubuntu Bold 9'
)

npm_install=(
  eslint
  prettier
  pyright
  typescript
  tree-sitter-cli
  typescript-language-server
)

pip_install=(
  flake8
  jedi-language-server
  pylsp-mypy
  python-lsp-black
  python-lsp-isort
  python-lsp-server"[all]"
)

lvim_install=(
    asm
    bash
    c
    cmake
    cpp
    css
    csv
    git_config
    git_rebase
    gitattributes
    gitcommit
    gitignore
    go
    htmldjango
    java
    javascript
    jq
    json
    json5
    latex
    lua
    make
    markdown
    markdown_inline
    nasm
    pug
    python
    rst
    rust
    scss
    sql
    ssh_config
    terraform
    tmux
    toml
    tsx
    typescript
    xml
    yaml
)

main() {
    if [[ $1 ]]; then
        subcommand_"$1"
    else
        subcommand_desktop_root
    fi
}

subcommand_desktop_root() {
    add_ppas_and_update
    install_packages

    install_i3_cinnamon
    usermod -aG docker "$username"

    switch_to_user
}

add_ppas_and_update() {
    apt-get update
    apt-get install -y ca-certificates curl gnupg lsb-release apt-transport-https ca-certificates curl gnupg-agent software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"

    apt-get update

    dpkg --configure -a
}

install_packages() {
    apt-get install -y debconf-utils
    apt-get upgrade -y
    apt-get install "${install_list[@]}" -y
    apt-get autoremove -y

    #wget https://github.com/wagoodman/dive/releases/download/v0.9.1/dive_0.9.1_linux_amd64.deb
    #sudo apt install dive_0.9.1_linux_amd64.deb
    #rm dive_0.9.1_linux_amd64.deb
}

install_i3_cinnamon() {
    (
        cd "$root/vendor/i3-cinnamon";
        make install
    )
}

switch_to_user() {
    su "$username" -c "bash $BASH_SOURCE desktop_user"
}

subcommand_desktop_user() {
    export PATH="$PATH:$HOME/.local/bin"
    set_gsettings
    set_dconf
    configure_dirs
    install_asdf
    install_cargo
    install_npm_packages
    install_lvim
}

set_gsettings() {
    local i

    local a=("${gsettings_values[@]}")
    for (( i=0; i<"${#a[@]}"; i+=3)); do
        gsettings set "${a[i]}" "${a[i+1]}" "${a[i+2]}"
    done

    export PATH="$HOME/.dotfiles/bin:$PATH"
    tm 11
    tm gruv
}

set_dconf() {
    local profile=':b1dcc9dd-5262-4d8d-a863-c897e6d979b9'
    dconf write /org/gnome/terminal/legacy/profiles:/"$profile"/scrollbar-policy '"never"'
    dconf write /org/gnome/terminal/legacy/default-show-menubar false
}

configure_dirs() {
    cd

    # Delete annyoing home dir structure.
    rm -fr "${unnecessary_files[@]}"
    cd -

    # Create main dirs.
    mkdir -p ~/eth ~/pro
}

install_asdf() {
    rm -fr "$HOME/.asdf"
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.9.0
    . "$HOME"/.asdf/asdf.sh
    echo '. "$HOME"/.asdf/asdf.sh' >> ~/.bashrc

    asdf uninstall nodejs | true
    asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
    asdf install nodejs latest
    asdf global nodejs latest
    asdf local nodejs latest

    npm install -g yarn

    asdf uninstall python | true
    asdf plugin add python https://github.com/danhper/asdf-python.git
    asdf install python 3.12.8
    asdf global python 3.12.8
    asdf local python 3.12.8

    pip install -U "${pip_install[@]}"

    curl -LsSf https://astral.sh/uv/install.sh | sh

    uv tool install ruff

    curl -sSL https://install.python-poetry.org | python3 -

    asdf uninstall kubectl | true
    asdf plugin-add kubectl https://github.com/asdf-community/asdf-kubectl.git
    asdf install kubectl latest
    asdf global kubectl latest
    asdf local kubectl latest

    asdf uninstall lazygit | true
    asdf plugin add lazygit https://github.com/nklmilojevic/asdf-lazygit.git
    asdf install lazygit latest
    asdf global lazygit latest

}

install_cargo() {
  sh <(curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs) -y --no-modify-path
}

install_lvim() {
  pkill nvim | true
  sleep 3
  rm -rf ~/.local/share/{lunarvim,lvim,nvim} ~/.config/{nvim,lvim} ~/.cache/nvim
  mkdir -p ~/.local/bin
  wget https://github.com/neovim/neovim/releases/download/stable/nvim.appimage -O- > ~/.local/bin/nvim
  chmod +x ~/.local/bin/nvim

  mkdir -p ~/.local/share/{nvim,lvim,lunarvim}
  chmod -R u+w ~/.local/share/{nvim,lvim,lunarvim}

  if [[ -f ~/.cargo/env ]]; then
    # shellcheck disable=SC1090
    . ~/.cargo/env
  fi

  bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh) --overwrite --yes

  lvim --headless "+TSInstallSync ${lvim_install[*]}" +qa

  ln -sfn "$HOME/.dotfiles/config/lvim/config.lua" "$HOME/.config/lvim/config.lua"
}

install_npm_packages() {
  npm install -g "${npm_install[@]}"
}

main "$@"
