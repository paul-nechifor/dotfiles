#!/bin/bash

set -e

root=$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)

username="$(getent passwd 1000 | cut -d: -f1)"
export DEBIAN_FRONTEND=noninteractive

install_list=(
    apt-file
    arandr
    bridge-utils # For networking.
    build-essential # Important for compiling packages.
    cloc # Counts lines of code.
    cryptsetup
    curl
    dconf-editor
    dkms
    dos2unix
    exfat-fuse # To read SD card filesystems.
    exfat-utils # To read SD card filesystems.
    festival
    git
    grc
    htop # Better than top.
    keychain
    libmad0
    libmtp-common
    libmtp-dev
    libmtp-runtime
    libmtp9
    lm-sensors
    mpg321
    mtp-tools
    nasm
    nfs-common
    openvpn
    p7zip
    p7zip-full
    python-dev # Needed to build some Python packages.
    python-jedi
    python-pip
    python3-dev # Needed to build some Python packages.
    rename
    silversearcher-ag
    tig
    tmux
    tree
    vim
)

main() {
    apt-get update
    apt-get install -y debconf-utils
    apt-get upgrade -y
    apt-get install "${install_list[@]}" -y
    apt-get autoremove
}

main "$@"
