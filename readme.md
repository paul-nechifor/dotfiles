# Dotfiles

![cover](screenshot.png)

My personal Linux dotfiles.

## Usage

To install everything on a new computer:

    sudo true; wget -qO- https://gitlab.com/paul-nechifor/dotfiles/raw/master/install.sh | bash -s - infect && . ~/.bashrc

After that, you can update everything with:

    infect

Locally, just run:

    sudo true; ./install.sh

## TODO

- Run `setxkbmap ro; xmodmap ~/.Xmodmap` every minute.

- Use https://github.com/mitsuhiko/unp .

- Start `nm-manager` in i3.

- Add `sass-lint` to Syntastic

- Use `grep` if `ack` or `ag` don't exist.

- Fix Syntastic conf.

- Use proselint in Syntastic.

- Add a script for which program is using which port.

## Nix usage

Install a package:

    nix-env -i vim

Remove a package:

    nix-env -e firefox

Update all packages:

    nix-env -u

## License

AGPL
