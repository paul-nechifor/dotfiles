#!/bin/bash

# # Bash run commands

# ## System detection
#
# This exports some boolean variables that are later used to customise behaviour
# based on the detected OS and distribution.
export is_vagrant; is_vagrant=$([[ -e /vagrant ]] && echo 1)
alias colourify="grc -es --colour=auto"
export is_ubuntu; is_ubuntu=$(grep -q Ubuntu /etc/issue && echo 1)
export is_mint; is_mint=$(grep -q Mint /etc/issue && echo 1)
export is_centos; is_centos=$(grep -q CentOS /etc/issue && echo 1)
export own_computer; own_computer=$([[ ( $is_ubuntu || $is_mint ) && ! $is_vagrant ]] && echo 1)

# ## Bash settings

if [[ $TERM != "dumb" ]]; then
  # Set vi mode.
  set -o vi

  # Use arrow keys for searching in the history.
  bind '"\e[A":history-search-backward'
  bind '"\e[B":history-search-forward'

  # Don't match hidden files when tab-completing file names.
  bind 'set match-hidden-files off'
fi

# Append to `~/.bash_history`.
shopt -s histappend

# Enable using `**` like in `ack word **/*.py`.
shopt -s globstar 2>/dev/null

# Enable using `!(readme.md)` to match all files that aren't `readme.md`.
shopt -s extglob

# Fixes some resizing weirdness.
shopt -s checkwinsize

# Remember an incredible amount of commands in the history.
export HISTSIZE=1000000
export HISTCONTROL=ignoreboth
export HISTFILESIZE=1000000000

# Add a timestamp to every history command.
export HISTTIMEFORMAT="%F %T"

# ## Bash prompt

PS1=
ucolor=$(if [[ $(id -u) -eq 0 ]]; then echo 31; else echo 32; fi)
# Only include my username if it's not one I expect.
if [[ ! ( $(whoami) =~ (^p$|^pnechifor$|^vagrant$|^root$) ) ]]; then
  PS1+='\[\e[0;'"$ucolor"'m\]\u@\[\e[0m\]'
fi
# Only include the host I'm not directly on my computer.
if [[ ! $own_computer ]]; then
  PS1+='\[\e[1;'"$ucolor"'m\]\h\[\e[0;30m\]:\[\e[0m\]'
fi
PS1+='\[\e[0;'"$ucolor"'m\]$(get_home_relative_path)\[\e[0m\] \n'
if [[ $TERM != "dumb" ]]; then
  PS1+='\[\e[1;'"$ucolor"'m\]● \[\e[0m\] '
else
  PS1+='\[\e[1;'"$ucolor"'m\]$ \[\e[0m\] '
fi
unset ucolor

# ## Environment variables

export LESS_TERMCAP_mb; LESS_TERMCAP_mb=$(printf "\e[1;37m")
export LESS_TERMCAP_md; LESS_TERMCAP_md=$(printf "\e[1;37m")
export LESS_TERMCAP_me; LESS_TERMCAP_me=$(printf "\e[0m")
export LESS_TERMCAP_se; LESS_TERMCAP_se=$(printf "\e[0m")
export LESS_TERMCAP_so; LESS_TERMCAP_so=$(printf "\e[1;47;30m")
export LESS_TERMCAP_ue; LESS_TERMCAP_ue=$(printf "\e[0m")
export LESS_TERMCAP_us; LESS_TERMCAP_us=$(printf "\e[0;36m")

man() {
  env \
  LESS_TERMCAP_mb="$(printf "\e[1;31m")" \
  LESS_TERMCAP_md="$(printf "\e[1;31m")" \
  LESS_TERMCAP_me="$(printf "\e[0m")" \
  LESS_TERMCAP_se="$(printf "\e[0m")" \
  LESS_TERMCAP_so="$(printf "\e[1;44;33m")" \
  LESS_TERMCAP_ue="$(printf "\e[0m")" \
  LESS_TERMCAP_us="$(printf "\e[1;32m")" \
  man "$@"
}

# Load the colors to be used in `ls`.
eval "$(dircolors ~/.dircolors)"

export EDITOR="lvim"

export TERM="screen-256color"

export PATH="$HOME/.dotfiles/bin:$PATH:$HOME/.local/bin"

if [[ -d $HOME/local/.ownbin ]]; then
  export PATH="$HOME/local/.ownbin/bin:$PATH"
  export LD_LIBRARY_PATH="$HOME/local/.ownbin/lib:$LD_LIBRARY_PATH"
fi

if [[ -d $HOME/.ownbin ]]; then
  export PATH="$HOME/.ownbin/bin:$PATH"
  export LD_LIBRARY_PATH="$HOME/.ownbin/lib:$LD_LIBRARY_PATH"
fi

export LIBRARY_PATH="$LD_LIBRARY_PATH"

export PYTHONSTARTUP="$HOME/.pythonrc"

export GOPATH="$HOME/.go"

export PATH="$PATH:$GOPATH/bin"

mkdir -p "$GOPATH" &>/dev/null

# ## Completion

[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

# ## File system management
alias ls="ls --color=auto"
alias la="l -A"
if [[ $is_centos ]]; then
  alias tree="tree -Cvh --dirsfirst"
else
  alias tree="tree -Cvh --du --dirsfirst"
fi
df_args="-ah --si -T --total -xcgroup -xsquashfs -xsysfs -xproc -xsecurityfs -xcgroup2 -xpstore -xmqueue -xdebugfs -xfusectl -xconfigfs -xdevpts -xhugetlbfs -xfuseblk -xbinfmt_misc -xfuse.gvfsd-fuse -xautofs -xtmpfs -xdevtmpf"
if which grc &>/dev/null; then
  alias df="colourify df $df_args"
else
  alias df="df $df_args"
fi

# Define lazy level commands for `tree`: `tree1`, `tree2`, ...
for i in {1..6}; do
  # shellcheck disable=SC2139
  alias "tree$i=tree -L $i"
done

# Go to a directory and delete all pyc files in there.
change_dir() {
  cd "$@" && (rm ./*.pyc &>/dev/null || true) && l
}

o() {
  if [[ "$1" == '-' || $# -eq 0 || -d "$1" ]]; then
    change_dir "$@"
  else
    command lvim -p "$@"
  fi
}

z() {
  local file="$HOME/pro/$1"
  if [[ -f $file && -x $file ]]; then
      "$file"
  else
      change_dir "$file"
  fi
}

_z() {
  local cur="${COMP_WORDS[COMP_CWORD]}"
  # shellcheck disable=SC2010
  COMPREPLY=( $(compgen -W "$(ls ~/pro | grep "^$cur")" ) )
}

complete -F _z -o dirnames z

# Create and go to a directory (possibly nested).
dc() {
  mkdir -p "$@"
  local last_dir="${*: -1}"
  cd "$last_dir" || true
}

# Create aliases for going up, i.e. `..`=`cd ..`, `...`=`cd ../..`, &c.
for i in {1..9}; do
  # shellcheck disable=SC2139,SC2030,SC2031
  alias "..$(for ((j=1; j < i; j += 1)); do echo -n .; done)=change_dir ..$(
    for ((j=1; j < i; j += 1)); do
      echo -n '/..'
    done
  )"
done

a() {
  separator —

  if which ag &>/dev/null; then
    ag --color "$@" | cut -c1-400
  else
    ack --color "$@" | cut -c1-400
  fi
}

docker-compose-old() {
  local file

  for arg in "$@"; do
    if [[ $arg == -f ]]; then
      command docker-compose-old "$@"
      return
    fi
  done

  file="$(
    # shellcheck disable=SC2012
    ls {docker-,}compose.y{a,}ml \
        docker/{docker-,}compose.y{a,}ml \
        2>/dev/null | head -n1
  )"

  if echo "$file" | grep / &>/dev/null; then
    command docker-compose-old -f "$file" "$@"
  else
    command docker-compose-old "$@"
  fi
}

# Replace `top` with `htop` if it exits.
if which htop &>/dev/null; then
  alias top="htop"
fi

get_home_relative_path() {
  local wd; wd="$(readlink -f "$(pwd)")"
  local home; home="$(readlink -f "$(eval echo ~"$(whoami)")")"
  sed "s#^$home/##" <<<"$wd"
}

p() {
    echo -n "$(whoami) $(hostname)$(tput setaf 0):$(tput setaf 12)"
    echo "$(get_home_relative_path)$(tput sgr0)"
}

rm() {
  command rm "$@"
  # If everything that's deleted is from the current dir then list the dir so I
  # can know what's left.
  if ! echo "$@" | grep / &>/dev/null; then
    separator —
    l
  fi
}

alias ipython="ipython --no-banner --no-confirm-exit"
alias vim="lvim -p"
alias sudo="sudo -E"
alias gdb="gdb -q"

if [[ -e ~/.work_email ]]; then
    git config --global user.email "$(cat ~/.work_email)"
else
    git config --global user.email "paul@nechifor.net"
fi

gsub() {
  git submodule sync --recursive
  git submodule foreach --recursive git fetch
  git submodule update --init --recursive
}

if [[ $own_computer ]]; then
  alias am="alsamixer"
  alias b="thunar . >/dev/null 2>&1 &"
  alias t="gnome-terminal >/dev/null 2>&1 &"
fi

if [[ -e $HOME/.asdf ]]; then
  . "$HOME"/.asdf/asdf.sh
  . "$HOME"/.asdf/completions/asdf.bash
fi

if [[ -e $HOME/.poetry ]]; then
  . "$HOME"/.poetry/env
fi

get_latest_python_bin_dir() {
  if test -e "$HOME"/.asdf/installs/python/*; then
    (
      cd "$HOME"/.asdf/installs/python/
      echo "$(pwd)/$(
        ls | tr . ' ' |
        sort -sk 3n | sort -sk 2n | sort -sk 1n |
        tail -n1 | tr ' ' .
      )/bin"
    )
  fi
}

if [[ get_latest_python_bin_dir ]]; then
  export PATH="$(get_latest_python_bin_dir):$PATH"
fi

alias egrep="egrep --color=auto"
alias grep="grep --color=auto"

if which grc &>/dev/null; then
  alias blkid='colourify blkid'
  alias configure='colourify ./configure'
  alias diff='colourify diff'
  alias docker='colourify docker'
  alias docker-machine='colourify docker-machine'
  alias env='colourify env'
  alias free='colourify free -h'
  alias fdisk='colourify fdisk'
  alias findmnt='colourify findmnt'
  #alias make='colourify make'
  alias gcc='colourify gcc'
  alias g++='colourify g++'
  alias id='colourify id'
  alias ip='colourify ip'
  alias iptables='colourify iptables'
  alias as='colourify as'
  alias gas='colourify gas'
  alias ld='colourify ld'
  alias lsof='colourify lsof'
  alias lsblk='colourify lsblk'
  alias lspci='colourify lspci'
  alias netstat='colourify netstat'
  alias ping='colourify ping'
  alias traceroute='colourify traceroute'
  alias traceroute6='colourify traceroute6'
  alias head='colourify head'
  alias tail='colourify tail'
  alias dig='colourify dig'
  alias mount='colourify mount'
  alias ps='colourify ps'
  alias mtr='colourify mtr'
  alias semanage='colourify semanage'
  alias getsebool='colourify getsebool'
  alias ifconfig='colourify ifconfig'
fi

alias less="less -FiSsr"

# Reinstall the dotfiles from the repo.
alias infect="sudo true && wget -qO- https://gitlab.com/paul-nechifor/dotfiles/raw/master/install.sh | bash -s - infect && . ~/.bashrc"

# Recursively reset all files in the current dir to 644 for normal and 755 for
# dirs.
alias resetmod="find . -type f -exec chmod 644 {} + ; find . -type d -exec chmod 755 {} +"

alias c=cmus

alias k=kubectl

# ## Fun things
3men() {
  festival --tts <<EOF
And we sit there, by its margin while the moon, who loves it too, stoops down to
kiss it with a sister's kiss, and throws her silver arms around it clingingly;
and we watch it as it flows, ever singing, ever whispering, out to meet its
king, the sea -- till our voices die away in silence, and the pipes go out --
till we, commonplace, everyday young men, feel strangely full of thoughts, half
sad, half sweet, and do not care or want to speak -- till we laugh, and rising,
knock the ashes from our burnt-out pipes, and say "Good night" and, lulled by
the lapping water and the rustling trees, we fall asleep beneath the great,
still stars, and dream that the world is young again -- young and sweet as she
used to be ere the centuries of fret and care had made old her loving heart --
sweet as she was in those bygone days when a new-made mother, she nursed us, her
children, upon her own deep breast -- ere the wiles of painted civilization had
lured us away from her fond arms, and the poisoned sneers of artificiality had
made us ashamed of the simple life we led with her, and the simple, stately home
where mankind was born so many thousands of years ago.
EOF
}

# ## Local bashrc

if [[ -f ~/.bashrc-local ]]; then
  # shellcheck disable=SC1090
  . ~/.bashrc-local
fi

sshes=(
  "pi zmeu zmeu ."
  "pi micu micu ."
)

for x in "${sshes[@]}"; do
  parts=($x)
  eval "${parts[1]}() {
    ssh -t ${parts[0]}@${parts[2]} bash -lc '
      if tmux list-sessions &>/dev/null; then
        exec tmux -q a
      else
        cd ${parts[3]} &>/dev/null
        exec tmux -q
      fi
    '
  }"
done
unset sshes

if [[ -f ~/.cargo/env ]]; then
  # shellcheck disable=SC1090
  . ~/.cargo/env
fi

export NVM_DIR="$HOME/.nvm"
if [ -s "$NVM_DIR/nvm.sh" ]; then
  . "$NVM_DIR/nvm.sh"
  . "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH
