if [[ $TERM != "dumb" ]]; then
  set -o vi
  bind '"\e[A":history-search-backward'
  bind '"\e[B":history-search-forward'
  bind 'set match-hidden-files off'
fi

shopt -s histappend
shopt -s globstar 2>/dev/null
shopt -s extglob
shopt -s checkwinsize

export HISTSIZE=1000000
export HISTCONTROL=ignoreboth
export HISTFILESIZE=1000000000
export HISTTIMEFORMAT="%F %T"

export EDITOR="lvim"
export TERM="screen-256color"

unalias l

alias ls="ls --color=auto"
alias la="l -A"
alias tree="tree -Cvh --du --dirsfirst"
alias vim="lvim -p"
alias sudo="sudo -E"
alias egrep="egrep --color=auto"
alias grep="grep --color=auto"
alias less="less -FiSsr"

l() {
  ls --color=always --group-directories-first -hlG --si "$@" |
  tail --lines=+2
}

change_dir() {
  cd "$@" && l
}

o() {
  if [[ "$1" == '-' || $# -eq 0 || -d "$1" ]]; then
    change_dir "$@"
  else
    command lvim -p "$@"
  fi
}

a() {
  if which ag &>/dev/null; then
    ag --color "$@" | cut -c1-400
  else
    ack --color "$@" | cut -c1-400
  fi
}
