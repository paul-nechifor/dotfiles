lvim.builtin.alpha.active = false
lvim.builtin.terminal.open_mapping = "<c-t>"
lvim.builtin.treesitter.rainbow.enable = true
lvim.format_on_save.enabled = true
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"
lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
vim.opt.wrap = true -- wrap lines
-- set autochdir

lvim.builtin.treesitter.ensure_installed = {
  "comment",
  "markdown_inline",
  "regex",
  "bash",
  "c",
  "c_sharp",
  "cmake",
  "cpp",
  "css",
  "diff",
  "dockerfile",
  "git_config",
  "gitcommit",
  "gitignore",
  "go",
  "gomod",
  "html",
  "java",
  "javascript",
  "jq",
  "json",
  "jsonnet",
  "latex",
  "lua",
  "make",
  "php",
  "python",
  "rust",
  "scss",
  "terraform",
  "toml",
  "typescript",
  "vim",
  "yaml",
}

lvim.autocommands = {
  {
    { "ColorScheme" },
    {
      pattern = "*",
      callback = function()
        vim.api.nvim_set_hl(0, "DiffAdd", { bg = "#0a5c3a", underline = false, bold = true })
        vim.api.nvim_set_hl(0, "DiffDelete", { bg = "#712828", underline = false, bold = false })
      end,
    },
  },
}

lvim.plugins = {
  {
    "ggandor/leap.nvim",
    name = "leap",
    config = function()
      require("leap").add_default_mappings()
    end,
  },
  --"mrjones2014/nvim-ts-rainbow",
  "nvim-neotest/neotest",
  "nvim-neotest/neotest-python",
  "sindrets/diffview.nvim",
  --"tpope/vim-surround",
}

local lspconfig = require('lspconfig')
-- lspconfig.jedi_language_server.setup {}
lspconfig.tsserver.setup {}
lspconfig.pylsp.setup {
  settings = {
    pylsp = {
      plugins = {
        -- formatter options
        black = { enabled = false },
        autopep8 = { enabled = false },
        yapf = { enabled = false },
        -- linter options
        pylint = { enabled = false },
        pyflakes = { enabled = false },
        pycodestyle = { enabled = false },
        -- type checker
        pylsp_mypy = { enabled = true },
        -- auto-completion options
        jedi_completion = { fuzzy = true },
        -- import sorting
        pyls_isort = { enabled = false },
        rope_autoimport = { enabled = false },

        ruff = { enabled = true },
      },
    },
  },
  flags = {
    debounce_text_changes = 200,
  },
}


local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  { command = "black",    filetype = { "python" } },
  { command = "ruff",     filetype = { "python" },                                                        args = { "--fix" } },
  { command = "prettier", filetype = { "javascript", "javascriptreact", "typescript", "typescriptreact" } },
  --{ command = "isort",    filetype = { "python" } },
  --{ command = "autopep8", filetype = { "python" } },
}

local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  { command = "eslint", filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" } },
  { command = "ruff",   filetypes = { "python" } },
  -- { command = "flake8", filetypes = { "python" } },
  -- { command = "mypy",   filetypes = { "python" } },
  { command = "tsc",    filetypes = { "typescript", "typescriptreact" } },
  -- { command = "vulture", filetypes = { "python" } },
}
