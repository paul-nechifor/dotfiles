import os
import sys

# Coloured prompt
if os.getenv('TERM') in ('xterm', 'vt100', 'rxvt', 'Eterm', 'putty'):
    try:
        import readline
    except ImportError:
        sys.ps1 = '\033[0;32m>>> \033[0m'
        sys.ps2 = '\033[0;32m... \033[0m'
    else:
        sys.ps1 = '\001\033[0;32m\002>>> \001\033[0m\002'
        sys.ps2 = '\001\033[0;32m\002... \001\033[0m\002'

# Completion!
try:
    import readline  # NOQA
except ImportError:
    print("Module readline not available.")
else:
    # persistent history
    histfile = os.path.expanduser('~/.pythonhistory')
    try:
        readline.read_history_file(histfile)
    except IOError:
        pass
    import atexit
    atexit.register(readline.write_history_file, histfile)
    del histfile, atexit
    # tab completion
    try:
        sys.path.append(os.path.join(os.getenv('HOME'), 'src', 'rlcompleter2'))
        import rlcompleter2
        rlcompleter2.setup()
        del rlcompleter2
    except ImportError:
        import rlcompleter
        readline.parse_and_bind("tab: complete")
        del rlcompleter
    del readline

from pprint import pprint as p  # NOQA

try:
    import glob

    def all_files(path=None, recursive=True):
        paths = glob.glob(path or '**/*', recursive=recursive)
        return sorted(filter(os.path.isfile, paths))
except ImportError:
    pass
